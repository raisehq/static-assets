(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('react'), require('semantic-ui-react'), require('styled-components'), require('react-spring'), require('moment'), require('axios'), require('react-chartjs-2'), require('web3-utils'), require('numeral'), require('react-content-loader'), require('pampy'), require('lodash-es/cloneDeep')) :
  typeof define === 'function' && define.amd ? define(['exports', 'react', 'semantic-ui-react', 'styled-components', 'react-spring', 'moment', 'axios', 'react-chartjs-2', 'web3-utils', 'numeral', 'react-content-loader', 'pampy', 'lodash-es/cloneDeep'], factory) :
  (global = global || self, factory(global['@raisehq/components'] = {}, global.React, global.semanticUiReact, global.styled, global.reactSpring, global.moment, global.axios, global.reactChartjs2, global.web3Utils, global.numeral, global.ContentLoader, global.pampy, global.cloneDeep));
}(this, (function (exports, React, semanticUiReact, styled, reactSpring, moment, axios, reactChartjs2, web3Utils, numeral, ContentLoader, pampy, cloneDeep) { 'use strict';

  var React__default = 'default' in React ? React['default'] : React;
  styled = styled && styled.hasOwnProperty('default') ? styled['default'] : styled;
  moment = moment && moment.hasOwnProperty('default') ? moment['default'] : moment;
  axios = axios && axios.hasOwnProperty('default') ? axios['default'] : axios;
  numeral = numeral && numeral.hasOwnProperty('default') ? numeral['default'] : numeral;
  ContentLoader = ContentLoader && ContentLoader.hasOwnProperty('default') ? ContentLoader['default'] : ContentLoader;
  cloneDeep = cloneDeep && cloneDeep.hasOwnProperty('default') ? cloneDeep['default'] : cloneDeep;

  function _extends() {
    _extends = Object.assign || function (target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

    return _extends.apply(this, arguments);
  }

  function _objectWithoutPropertiesLoose(source, excluded) {
    if (source == null) return {};
    var target = {};
    var sourceKeys = Object.keys(source);
    var key, i;

    for (i = 0; i < sourceKeys.length; i++) {
      key = sourceKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      target[key] = source[key];
    }

    return target;
  }

  function _taggedTemplateLiteralLoose(strings, raw) {
    if (!raw) {
      raw = strings.slice(0);
    }

    strings.raw = raw;
    return strings;
  }

  var size = {
    mobileS: '321px',
    mobileM: '375px',
    mobileL: '425px',
    tablet: '768px',
    laptop: '1024px',
    laptopM: '1430px',
    laptopL: '1439px',
    laptopXL: '1440px',
    desktop: '2560px'
  };
  var device = {
    mobileS: "(min-width: " + size.mobileS + ")",
    mobileM: "(min-width: " + size.mobileM + ")",
    mobileL: "(min-width: " + size.mobileL + ")",
    tablet: "(min-width: " + size.tablet + ")",
    laptop: "(min-width: " + size.laptop + ")",
    laptopM: "(min-width: " + size.laptopM + ")",
    laptopL: "(min-width: " + size.laptopL + ")",
    laptopXL: "(min-width: " + size.laptopXL + ")",
    desktop: "(min-width: " + size.desktop + ")",
    desktopL: "(min-width: " + size.desktop + ")"
  };

  function _templateObject30() {
    var data = _taggedTemplateLiteralLoose(["\n  &&& {\n    position: absolute;\n    top: 0;\n    right: 0;\n    color: white;\n  }\n"]);

    _templateObject30 = function _templateObject30() {
      return data;
    };

    return data;
  }

  function _templateObject29() {
    var data = _taggedTemplateLiteralLoose(["\n  position: absolute;\n  right: 105px;\n  font-size: 9px;\n  background: black;\n  width: 18px;\n  height: 18px;\n  border-radius: 36px;\n"]);

    _templateObject29 = function _templateObject29() {
      return data;
    };

    return data;
  }

  function _templateObject28() {
    var data = _taggedTemplateLiteralLoose(["\n  width: 2px;\n  height: 35px;\n  background: #ecedee;\n  margin: 0px;\n"]);

    _templateObject28 = function _templateObject28() {
      return data;
    };

    return data;
  }

  function _templateObject27() {
    var data = _taggedTemplateLiteralLoose(["\n  width: 100%;\n  height: 1px;\n  background: #ecedee;\n  margin: 0px;\n"]);

    _templateObject27 = function _templateObject27() {
      return data;
    };

    return data;
  }

  function _templateObject26() {
    var data = _taggedTemplateLiteralLoose(["\n  width: 80px;\n  height: 20px;\n  color: #fff;\n  position: absolute;\n  top: 15px;\n  right: 15px;\n  text-align: center;\n  padding: 3px 0 3px 0;\n  font-weight: bold;\n  background: ", ";\n  border-radius: 30px;\n  font-size: 12px;\n  line-height: 15px;\n"]);

    _templateObject26 = function _templateObject26() {
      return data;
    };

    return data;
  }

  function _templateObject25() {
    var data = _taggedTemplateLiteralLoose(["\n  width: 100%;\n  height: 23px;\n  background: #ecedee;\n  position: relative;\n  overflow: hidden;\n  border-radius: 4px;\n\n  &&:before {\n    content: '';\n    position: absolute;\n    width: ", "%;\n    height: 100%;\n    top: 0;\n    border-radius: 4px;\n    left: 0;\n    background: ", ";\n  }\n"]);

    _templateObject25 = function _templateObject25() {
      return data;
    };

    return data;
  }

  function _templateObject24() {
    var data = _taggedTemplateLiteralLoose(["\n  font-weight: bold;\n  color: white;\n  position: absolute;\n  font-size: 14px;\n  top: 2px;\n  left: 5px;\n"]);

    _templateObject24 = function _templateObject24() {
      return data;
    };

    return data;
  }

  function _templateObject23() {
    var data = _taggedTemplateLiteralLoose(["\n  width: 90%;\n  height: 10px;\n  background: #ecedee;\n  position: relative;\n  overflow: hidden;\n\n  &&:before {\n    content: '';\n    position: absolute;\n    width: ", "%;\n    height: 100%;\n    top: 0;\n    left: 0;\n    background: ", ";\n  }\n"]);

    _templateObject23 = function _templateObject23() {
      return data;
    };

    return data;
  }

  function _templateObject22() {
    var data = _taggedTemplateLiteralLoose(["\n  color: #3c4251;\n  font-size: 16px;\n  font-weight: bold;\n  line-height: 28px;\n"]);

    _templateObject22 = function _templateObject22() {
      return data;
    };

    return data;
  }

  function _templateObject21() {
    var data = _taggedTemplateLiteralLoose(["\n  color: #5a5a5a;\n  font-size: 10px;\n  font-weight: lighter;\n  margin-bottom: 4px;\n  line-height: 14px;\n"]);

    _templateObject21 = function _templateObject21() {
      return data;
    };

    return data;
  }

  function _templateObject20() {
    var data = _taggedTemplateLiteralLoose(["\n  margin-top: 0px;\n  margin-bottom: 5px;\n"]);

    _templateObject20 = function _templateObject20() {
      return data;
    };

    return data;
  }

  function _templateObject19() {
    var data = _taggedTemplateLiteralLoose(["\n  color: #5a5a5a;\n  font-size: 14px;\n  font-weight: bold;\n  line-height: 21px;\n"]);

    _templateObject19 = function _templateObject19() {
      return data;
    };

    return data;
  }

  function _templateObject18() {
    var data = _taggedTemplateLiteralLoose(["\n  display: flex;\n  margin-top: 20px;\n  margin-bottom: 12px;\n  align-self: flex-end;\n  justify-content: center;\n  align-items: center;\n"]);

    _templateObject18 = function _templateObject18() {
      return data;
    };

    return data;
  }

  function _templateObject17() {
    var data = _taggedTemplateLiteralLoose(["\n  color: #3c4251;\n  font-size: ", ";\n  font-weight: bold;\n  line-height: 32px;\n"]);

    _templateObject17 = function _templateObject17() {
      return data;
    };

    return data;
  }

  function _templateObject16() {
    var data = _taggedTemplateLiteralLoose(["\n  display: flex;\n  color: #5a5a5a;\n  font-size: 12px;\n  font-weight: lighter;\n  line-height: 14px;\n  margin-bottom: 12px;\n  margin-top: 20px;\n"]);

    _templateObject16 = function _templateObject16() {
      return data;
    };

    return data;
  }

  function _templateObject15() {
    var data = _taggedTemplateLiteralLoose(["\n  margin-top: 20px;\n  margin-bottom: 12px;\n"]);

    _templateObject15 = function _templateObject15() {
      return data;
    };

    return data;
  }

  function _templateObject14() {
    var data = _taggedTemplateLiteralLoose(["\n  font-size: 10px;\n  font-weight: bold;\n  margin-left: 6px;\n"]);

    _templateObject14 = function _templateObject14() {
      return data;
    };

    return data;
  }

  function _templateObject13() {
    var data = _taggedTemplateLiteralLoose(["\n  font-size: ", ";\n  color: #5c5d5d;\n  text-align: center;\n"]);

    _templateObject13 = function _templateObject13() {
      return data;
    };

    return data;
  }

  function _templateObject12() {
    var data = _taggedTemplateLiteralLoose(["\n  color: ", ";\n  font-size: 14px;\n  font-weight: bold;\n  text-align: center;\n"]);

    _templateObject12 = function _templateObject12() {
      return data;
    };

    return data;
  }

  function _templateObject11() {
    var data = _taggedTemplateLiteralLoose(["\n  flex: ", ";\n  margin: 20px 0px 0px 0px;\n  ", "\n  text-align: center;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n"]);

    _templateObject11 = function _templateObject11() {
      return data;
    };

    return data;
  }

  function _templateObject10() {
    var data = _taggedTemplateLiteralLoose(["\n  display: flex;\n  margin: 20px 0px;\n  justify-content: ", ";\n  ", "\n  ", "\n  ", "\n  ", "\n  ", "\n  box-sizing: border-box;\n  flex-wrap: wrap;\n"]);

    _templateObject10 = function _templateObject10() {
      return data;
    };

    return data;
  }

  function _templateObject9() {
    var data = _taggedTemplateLiteralLoose(["\n  padding: 20px;\n  position: relative;\n  height: ", ";\n  display: flex;\n  flex-flow: column;\n  justify-content: flex-start;\n  padding-top: ", ";\n  &&& > .logoWrap {\n    position: absolute;\n    top: -35px;\n    left: 14px;\n\n  }\n  &&& > ", " {\n    position: absolute;\n    top: 10px;\n    right: 8px;\n  }\n"]);

    _templateObject9 = function _templateObject9() {
      return data;
    };

    return data;
  }

  function _templateObject8() {
    var data = _taggedTemplateLiteralLoose(["\n  max-height: 76px;\n  height: 76px;\n  color: #5a5a5a;\n  font-size: 14px;\n  display: block;\n  text-align: left;\n  line-height: 21px;\n"]);

    _templateObject8 = function _templateObject8() {
      return data;
    };

    return data;
  }

  function _templateObject7() {
    var data = _taggedTemplateLiteralLoose(["\n  color: #5a5a5a;\n  font-size: 14px;\n  font-weight: bold;\n  text-align: left;\n"]);

    _templateObject7 = function _templateObject7() {
      return data;
    };

    return data;
  }

  function _templateObject6() {
    var data = _taggedTemplateLiteralLoose(["\n  font-weight: bold;\n  color: #5a5a5a;\n  font-size: 14px;\n"]);

    _templateObject6 = function _templateObject6() {
      return data;
    };

    return data;
  }

  function _templateObject5() {
    var data = _taggedTemplateLiteralLoose(["\n  min-height: ", ";\n  border-radius: 6px;\n  background-color: #ffffff;\n  border: 1px solid #cfd0d4;\n  box-sizing: border-box;\n  position: relative;\n  display: flex;\n  flex-flow: column;\n  justify-content: flex-start;\n\n  max-width: ", " !important;\n  @media ", " {\n    width: ", " !important;\n  }\n  width: 100%;\n"]);

    _templateObject5 = function _templateObject5() {
      return data;
    };

    return data;
  }

  function _templateObject4() {
    var data = _taggedTemplateLiteralLoose(["\n  &&& {\n    width: 70px;\n    height: 70px;\n    background-color: white;\n    border-radius: 6px;\n    border: 1px solid #cfd0d4;\n  \n    background-position: center;\n    background-position-x: 0;\n    background-size: contain;\n    background-repeat: no-repeat;\n    background-image: ", ";\n  }\n"]);

    _templateObject4 = function _templateObject4() {
      return data;
    };

    return data;
  }

  function _templateObject3() {
    var data = _taggedTemplateLiteralLoose(["\n  width: 100%;\n  display: block;\n"]);

    _templateObject3 = function _templateObject3() {
      return data;
    };

    return data;
  }

  function _templateObject2() {
    var data = _taggedTemplateLiteralLoose(["\n  width: 100%;\n  height: 124px;\n  background-size: cover;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-image: ", ";\n  border-radius: 6px 6px 0 0;\n  border-bottom: 1px solid #cfd0d4;\n"]);

    _templateObject2 = function _templateObject2() {
      return data;
    };

    return data;
  }

  function _templateObject() {
    var data = _taggedTemplateLiteralLoose(["\n  position: relative;\n  display: flex;\n  align-items: center;\n"]);

    _templateObject = function _templateObject() {
      return data;
    };

    return data;
  }
  var GraphContainer =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject());
  var CardImageCrop =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject2(), function (_ref) {
    var src = _ref.src;
    return "url(" + src + ")";
  });
  var CardHref =
  /*#__PURE__*/
  styled.a(
  /*#__PURE__*/
  _templateObject3());
  var CardLogo =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject4(), function (_ref2) {
    var src = _ref2.src;
    return "url(" + src + ")";
  });
  var HeroCard =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject5(), function (_ref3) {
    var size = _ref3.size;
    return size || '335px';
  }, function (_ref4) {
    var width = _ref4.width;
    return width || '372px';
  }, device.laptop, function (_ref5) {
    var width = _ref5.width;
    return width || '372px';
  });
  var TimeLeft =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject6());
  var CardBorrowerTitle =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject7());
  var CardDescription =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject8());
  var CardContent =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject9(), function (_ref6) {
    var size = _ref6.size;
    return size || '100%';
  }, function (_ref7) {
    var logo = _ref7.logo;
    return logo ? '55px' : '0';
  }, TimeLeft);
  var Grid =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject10(), function (_ref8) {
    var spaceBetween = _ref8.spaceBetween;
    return spaceBetween ? 'space-between' : 'unset';
  }, function (_ref9) {
    var nobottom = _ref9.nobottom;
    return nobottom && 'margin-bottom: 0;';
  }, function (_ref10) {
    var notop = _ref10.notop;
    return notop && 'margin-top: 0;';
  }, function (_ref11) {
    var alignCenter = _ref11.alignCenter;
    return alignCenter && 'align-items: center;';
  }, function (_ref12) {
    var alignBottom = _ref12.alignBottom;
    return alignBottom && 'align-items: flex-end;';
  }, function (_ref13) {
    var alignTop = _ref13.alignTop;
    return alignTop && 'align-items: flex-start;';
  });
  var Row =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject11(), function (_ref14) {
    var small = _ref14.small;
    return small ? '1 0 25%' : '1 0 32.5%';
  }, function (_ref15) {
    var notop = _ref15.notop;
    return notop && 'margin-top: 0;';
  });
  var RowContent =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject12(), function (_ref16) {
    var contentColor = _ref16.contentColor;
    return contentColor ? contentColor : '#5a5a5a';
  });
  var RowTitle =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject13(), function (_ref17) {
    var big = _ref17.big;
    return big ? '14px' : '10px';
  });
  var GraphTitle =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject14());
  var Header =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject15());
  var HeaderTitle =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject16());
  var HeaderContent =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject17(), function (_ref18) {
    var fontSize = _ref18.fontSize;
    return fontSize || '26px';
  });
  var RoiHeader =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject18());
  var RoiContent =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject19());
  var SubHeader =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject20());
  var SubHeaderTitle =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject21());
  var SubHeaderContent =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject22());
  var Graph =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject23(), function (props) {
    return props.width * 100 / 90;
  }, function (props) {
    return props.color;
  });
  var ProgressPercent =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject24());
  var ProgressBar =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject25(), function (props) {
    return props.width;
  }, function (props) {
    return props.color;
  });
  var Badge =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject26(), function (_ref19) {
    var color = _ref19.color;
    return color;
  });
  var Separator =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject27());
  var Vertical =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject28());
  var InfoIcon =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject29());
  var InfoIconCmp =
  /*#__PURE__*/
  styled(semanticUiReact.Icon)(
  /*#__PURE__*/
  _templateObject30());

  var Right = function Right(x) {
    return {
      map: function map(f) {
        return Right(f(x));
      },
      // @ts-ignore
      fold: function fold(f, g) {
        return g(x);
      },
      inspect: function inspect() {
        return "Right(" + x + ")";
      }
    };
  };
  var Left = function Left(x) {
    return {
      map: function map() {
        return Left(x);
      },
      fold: function fold(f) {
        return f(x);
      },
      inspect: function inspect() {
        return "Left(" + x + ")";
      }
    };
  };
  var Either = {
    either: function either(coor) {
      return coor ? Right(true) : Left(false);
    }
  };

  var useGraphWidth = function useGraphWidth(ref, currentAmount, totalAmount) {
    var config = Either.either(ref);
    return config.fold(function () {
      return {
        width: 0,
        originalWidth: 0
      };
    }, function () {
      var width = ref.getBoundingClientRect().width - 50;
      var percent = currentAmount / totalAmount * 100;
      return {
        width: percent,
        originalWidth: width
      };
    });
  };

  var Context =
  /*#__PURE__*/
  React__default.createContext({});

  var BadgeComponent = function BadgeComponent(_ref) {
    var children = _ref.children,
        color = _ref.color;
    return React__default.createElement(Badge, {
      color: color
    }, children);
  };

  var RowComponent = function RowComponent(_ref2) {
    var title = _ref2.title,
        content = _ref2.content,
        contentColor = _ref2.contentColor,
        small = _ref2.small,
        big = _ref2.big,
        notop = _ref2.notop;
    return React__default.createElement(Row, {
      small: small,
      big: big,
      notop: notop
    }, React__default.createElement(RowContent, {
      contentColor: contentColor
    }, content), React__default.createElement(RowTitle, {
      big: big
    }, title));
  };

  var HeaderComponent = function HeaderComponent(_ref3) {
    var title = _ref3.title,
        amount = _ref3.amount,
        fontSize = _ref3.fontSize,
        rest = _objectWithoutPropertiesLoose(_ref3, ["title", "amount", "fontSize"]);

    return React__default.createElement(Header, Object.assign({}, rest), React__default.createElement(HeaderTitle, null, title), React__default.createElement(HeaderContent, {
      fontSize: fontSize
    }, amount));
  };

  var SubHeaderComponent = function SubHeaderComponent(_ref4) {
    var title = _ref4.title,
        amount = _ref4.amount,
        rest = _objectWithoutPropertiesLoose(_ref4, ["title", "amount"]);

    return React__default.createElement(SubHeader, Object.assign({}, rest), React__default.createElement(SubHeaderTitle, null, title), React__default.createElement(SubHeaderContent, null, amount));
  };

  var RoiHeaderComponent = function RoiHeaderComponent(_ref5) {
    var roi = _ref5.roi;
    return React__default.createElement(RoiHeader, null, React__default.createElement(RoiContent, null, roi + "ROI"));
  };

  var CardWrapper = function CardWrapper(_ref6) {
    var children = _ref6.children,
        size = _ref6.size,
        width = _ref6.width,
        props = _objectWithoutPropertiesLoose(_ref6, ["children", "size", "width"]);

    var graph = React__default.useRef(null);

    var _React$useState = React__default.useState({
      ref: null
    }),
        values = _React$useState[0],
        setValues = _React$useState[1];

    React__default.useEffect(function () {
      return setValues({
        ref: graph.current
      });
    }, []);
    return React__default.createElement(Context.Provider, {
      value: values
    }, React__default.createElement(HeroCard, Object.assign({
      className: "heroCard",
      ref: function ref(_ref7) {
        return graph.current = _ref7;
      }
    }, props, {
      size: size,
      width: width
    }), children));
  };

  var Card = CardWrapper;

  var GraphComponent = function GraphComponent(_ref8) {
    var color = _ref8.color,
        currentAmount = _ref8.currentAmount,
        totalAmount = _ref8.totalAmount;

    var _React$useContext = React__default.useContext(Context),
        ref = _React$useContext.ref;

    var config = useGraphWidth(ref, currentAmount, totalAmount);
    return React__default.createElement(GraphContainer, null, React__default.createElement(Graph, {
      color: color,
      width: config.width
    }), React__default.createElement(GraphTitle, null, Math.floor(config.width), "%"));
  };

  var ProgressComponent = function ProgressComponent(_ref9) {
    var color = _ref9.color,
        currentAmount = _ref9.currentAmount,
        totalAmount = _ref9.totalAmount;

    var _React$useContext2 = React__default.useContext(Context),
        ref = _React$useContext2.ref;

    var config = useGraphWidth(ref, currentAmount, totalAmount);
    return React__default.createElement(GraphContainer, null, React__default.createElement(ProgressBar, {
      color: color,
      width: config.width
    }), React__default.createElement(ProgressPercent, null, Math.floor(config.width), "%"));
  };

  var TooltipComponent = function TooltipComponent() {
    return React__default.createElement(semanticUiReact.Popup, {
      content: "blablabablalbabalabl",
      key: 2434324,
      trigger: React__default.createElement(InfoIcon, null, React__default.createElement(InfoIconCmp, {
        name: "info"
      }))
    });
  };

  var ContentWithLogo = function ContentWithLogo(_ref10) {
    var children = _ref10.children,
        logo = _ref10.logo,
        topRight = _ref10.topRight,
        size = _ref10.size,
        to = _ref10.to,
        className = _ref10.className,
        style = _ref10.style;
    var aProps = {
      href: undefined
    };

    if (to) {
      aProps.href = to;
    }

    return React__default.createElement(CardContent, {
      logo: logo,
      size: size,
      className: className,
      style: style
    }, logo && React__default.createElement("a", Object.assign({
      className: "logoWrap"
    }, aProps), React__default.createElement(CardLogo, {
      src: logo
    })), topRight && React__default.createElement(TimeLeft, null, topRight), children);
  };

  var CardImage = function CardImage(_ref11) {
    var src = _ref11.src,
        to = _ref11.to;

    if (to) {
      return React__default.createElement(CardHref, {
        href: to
      }, React__default.createElement(CardImageCrop, {
        src: src
      }));
    }

    return React__default.createElement(CardImageCrop, {
      src: src
    });
  };

  Card.BorrowerTitle = CardBorrowerTitle;
  Card.Description = CardDescription;
  Card.Image = CardImage;
  Card.Logo = CardLogo;
  Card.Content = ContentWithLogo;
  Card.Badge = BadgeComponent;
  Card.Row = RowComponent;
  Card.Grid = Grid;
  Card.Header = HeaderComponent;
  Card.SubHeader = SubHeaderComponent;
  Card.Graph = GraphComponent;
  Card.Progress = ProgressComponent;
  Card.Separator = Separator;
  Card.Vertical = Vertical;
  Card.Tooltip = TooltipComponent;
  Card.RoiHeader = RoiHeaderComponent;
  Card.TimeLeft = TimeLeft;

  function _templateObject2$1() {
    var data = _taggedTemplateLiteralLoose(["\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  color: #eeb345;\n  & > ", " {\n    width: 18px;\n    height: 18px;\n  }\n  & > div {\n    font-weight: bold;\n    font-size: 12px;\n  }\n"]);

    _templateObject2$1 = function _templateObject2() {
      return data;
    };

    return data;
  }

  function _templateObject$1() {
    var data = _taggedTemplateLiteralLoose([""]);

    _templateObject$1 = function _templateObject() {
      return data;
    };

    return data;
  }
  var CoinImage =
  /*#__PURE__*/
  styled(semanticUiReact.Image)(
  /*#__PURE__*/
  _templateObject$1());
  var CoinBox =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject2$1(), CoinImage);

  var Coin = function Coin(_ref) {
    var src = _ref.src,
        name = _ref.name;
    return React__default.createElement(CoinBox, null, React__default.createElement(CoinImage, {
      src: src
    }), name && React__default.createElement("div", null, name));
  };

  function _templateObject5$1() {
    var data = _taggedTemplateLiteralLoose(["\n  width: 56.293px;\n  height: 27.2px;\n  display: flex;\n  align-items: center;\n"]);

    _templateObject5$1 = function _templateObject5() {
      return data;
    };

    return data;
  }

  function _templateObject4$1() {
    var data = _taggedTemplateLiteralLoose(["\n  &&& {\n    align-items: center;\n    display: flex;\n    justify-content: center;\n    & > i:first-child {\n      margin-right: 10px;\n    }\n  }\n"]);

    _templateObject4$1 = function _templateObject4() {
      return data;
    };

    return data;
  }

  function _templateObject3$1() {
    var data = _taggedTemplateLiteralLoose(["\n  padding: 0px 20px 20px 20px;\n  ", ":first-child {\n    margin-bottom: 0px;\n  }\n"]);

    _templateObject3$1 = function _templateObject3() {
      return data;
    };

    return data;
  }

  function _templateObject2$2() {
    var data = _taggedTemplateLiteralLoose(["\n  padding: 40px 20px 0px 20px;\n"]);

    _templateObject2$2 = function _templateObject2() {
      return data;
    };

    return data;
  }

  function _templateObject$2() {
    var data = _taggedTemplateLiteralLoose(["\n  min-height: 470px;\n  max-width: 372px;\n  width: 100%;\n"]);

    _templateObject$2 = function _templateObject() {
      return data;
    };

    return data;
  }
  var InvestCardBody =
  /*#__PURE__*/
  styled(CardWrapper)(
  /*#__PURE__*/
  _templateObject$2());
  var CardContent$1 =
  /*#__PURE__*/
  styled(Card.Content)(
  /*#__PURE__*/
  _templateObject2$2());
  var CardBottom =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject3$1(), Card.Grid);
  var GraphButton =
  /*#__PURE__*/
  styled(semanticUiReact.Button)(
  /*#__PURE__*/
  _templateObject4$1());
  var SpacedDiv =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject5$1());

  function useEffectAsync(effect, destroy, inputs) {
    var hasDestroy = typeof destroy === 'function';
    React.useEffect(function () {
      var result;
      var mounted = true;
      var maybePromise = effect(function () {
        return mounted;
      });
      Promise.resolve(maybePromise).then(function (value) {
        result = value;
      });
      return function () {
        mounted = false;

        if (hasDestroy) {
          destroy(result);
        }
      };
    }, hasDestroy ? inputs : destroy);
  }

  var addDays = function addDays(datetime, days) {
    var date = new Date(datetime.getTime());
    date.setDate(date.getDate() + days);
    return date;
  };

  var getDates = function getDates(startDate, stopDate) {
    var dateArray = [];
    var currentDate = new Date(startDate.getTime());

    while (currentDate <= stopDate) {
      dateArray.push(currentDate);
      currentDate = addDays(currentDate, 1);
    }

    return dateArray;
  };
  var getClosestIndexByDate = function getClosestIndexByDate(dateList, targetDate) {
    var tempDiff = dateList.map(function (d) {
      return Math.abs(moment(d).diff(moment(targetDate)));
    });
    var index = tempDiff.indexOf(Math.min.apply(Math, tempDiff));
    return index;
  };
  var getAverage = function getAverage(arr) {
    return arr.reduce(function (p, c) {
      return p + c;
    }, 0) / arr.length;
  };

  /** Start of number formatting */

  var numeralFormat = '0,0.00';

  if (!numeral['locales']['hero']) {
    numeral.register('locale', 'hero', {
      delimiters: {
        thousands: '.',
        decimal: ','
      },
      abbreviations: {
        thousand: 'k',
        million: 'mm',
        billion: 'b',
        trillion: 't'
      },
      ordinal: function ordinal(number) {
        var b = number % 10;
        return b === 1 || b === 3 ? 'er' : b === 2 ? 'do' : b === 7 || b === 0 ? 'mo' : b === 8 ? 'vo' : b === 9 ? 'no' : 'to';
      },
      currency: {
        symbol: '€'
      }
    });
  }

  numeral.locale('hero');
  numeral.defaultFormat(numeralFormat);

  /* eslint-disable no-underscore-dangle */
  var chartBackground = {
    beforeDraw: function beforeDraw(chart, _easing) {
      if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
        var ctx = chart.chart.ctx;
        var chartArea = chart.chartArea;
        ctx.save();
        ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
        ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
        ctx.restore();
      }
    }
  };
  var todayVerticalLine = {
    afterDatasetsDraw: function afterDatasetsDraw(chart, _easing) {
      var meta = chart.getDatasetMeta(0);
      var x = meta.data[chart.options.lineAtIndex[0]]._model.x;
      var todayX;
      var todayOverX = 25;

      if (chart.options.lineAtIndex[0] + 1 <= chart.data.datasets[0].data.length / 2) {
        todayX = x + todayOverX;
      } else {
        todayX = x - todayOverX;
      }

      var topY = chart.scales['y-axis-0'].top;
      var bottomY = chart.scales['y-axis-0'].bottom; // draw line

      chart.ctx.save();
      chart.ctx.beginPath();
      chart.ctx.moveTo(x, topY);
      chart.ctx.lineTo(x, bottomY);
      chart.ctx.lineWidth = 1;
      chart.ctx.strokeStyle = '#ADADAD'; // write TODAY

      chart.ctx.textAlign = 'center';
      chart.ctx.fillStyle = '#7797AA';
      chart.ctx.fillText('Today', todayX, topY + 20);
      chart.ctx.stroke();
      chart.ctx.restore();
    }
  };

  var DAI_ADDRESS = '0x5d3a536e4d6dbd6114cc1ead35777bab948e3643';

  var Chart =
  /*#__PURE__*/
  require('react-chartjs-2');

  var datasetToGraph = function datasetToGraph(dataset, rgb, label, fill, borderWidth, dashed, pointHover, pointRadius) {
    return {
      label: label,
      fill: fill,
      lineTension: 0.4,
      backgroundColor: "rgba(" + rgb + ",0.4)",
      borderColor: "rgba(" + rgb + ",1)",
      borderCapStyle: 'butt',
      borderDash: !dashed ? [] : [10, 5],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "rgba(" + rgb + ",1)",
      pointBackgroundColor: "rgba(" + rgb + ",1)",
      pointBorderWidth: 0,
      pointRadius: 0,
      pointHoverRadius: pointRadius,
      borderWidth: !borderWidth ? 1 : borderWidth,
      pointHoverBackgroundColor: pointHover.length ? pointHover.map(function (x) {
        return x > 0 ? "rgba(" + rgb + ",1)" : "rgba(" + rgb + ",0)";
      }) : "rgba(" + rgb + ",1)",
      pointHoverBorderColor: pointHover.length ? pointHover.map(function (x) {
        return x > 0 ? "rgba(" + rgb + ",1)" : "rgba(" + rgb + ",0)";
      }) : "rgba(" + rgb + ",1)",
      pointHoverBorderWidth: pointHover.length ? pointHover : 2,
      pointHitRadius: 10,
      data: [].concat(dataset)
    };
  };

  var options = {
    fullCompoundDataset: [0],
    legend: {
      display: false,
      position: 'top',
      labels: {
        boxWidth: 22,
        fontSize: 12,
        padding: 5
      }
    },
    chartArea: {
      backgroundColor: 'rgba(248,248,248,1)'
    },
    lineAtIndex: [0],
    onHover: function onHover() {},
    layout: {
      padding: {
        left: 5,
        right: 5
      }
    },
    tooltips: {
      mode: 'index',
      intersect: false,
      enabled: false
    },
    hover: {
      mode: 'index',
      intersect: false
    },
    scales: {
      xAxes: [{
        ticks: {
          display: false
        },
        display: false
      }],
      yAxes: [{
        ticks: {
          display: false,
          min: 0,
          max: 21,
          stepSize: 1
        },
        display: false
      }]
    }
  };

  var getRaiseDataset = function getRaiseDataset(dates, auctionStart, auctionEnd, maxInterest, minInterest) {
    return dates.map(function (d) {
      return (maxInterest - minInterest) * Math.abs(d.valueOf() - auctionStart.valueOf()) / Math.abs(auctionEnd.valueOf() - auctionStart.valueOf()) + minInterest;
    });
  };

  var APRGraph = function APRGraph(_ref) {
    var maxInterestRate = _ref.maxInterestRate,
        minInterestRate = _ref.minInterestRate,
        auctionStartTimestamp = _ref.auctionStartTimestamp,
        auctionEndTimestamp = _ref.auctionEndTimestamp,
        currentAPR = _ref.currentAPR;

    var _useState = React.useState([0]),
        compoundDataset = _useState[0],
        setCompoundDataset = _useState[1];

    var _useState2 = React.useState([0]),
        fullCompoundDataset = _useState2[0],
        setFullCompoundDataset = _useState2[1];

    var _useState3 = React.useState(['0%', '0%']),
        _useState3$ = _useState3[0],
        currentLoanInterest = _useState3$[0],
        compoundInterest = _useState3$[1],
        setInterest = _useState3[1];

    var _useState4 = React.useState(new Date()),
        selectedDate = _useState4[0],
        setSelectedDate = _useState4[1];

    var medianCompoundRate = getAverage(fullCompoundDataset);
    var medianCompoundRateNumeral = numeral(medianCompoundRate / 100).format('0.00%');
    var maxInterest = Number(web3Utils.fromWei(maxInterestRate.toString())) * 12;
    var minInterest = minInterestRate ? Number(web3Utils.fromWei(minInterestRate.toString())) * 12 : 0;
    var dateStart = new Date(auctionStartTimestamp * 1000);
    var dateEnd = new Date(auctionEndTimestamp * 1000);
    var dateNow = new Date();
    var arrayDays = getDates(dateStart, dateEnd);
    var nowIndex = getClosestIndexByDate(arrayDays, dateNow);
    var raiseDataset = getRaiseDataset(arrayDays, dateStart, dateEnd, maxInterest, minInterest);
    var raiseGraphData = datasetToGraph(raiseDataset, '235,63,147', 'Raise', false, 3, false, [], 5);
    var compoundGraphData = datasetToGraph(compoundDataset, '119,151,170', 'Compound', true, 1, false, [], 3);
    var avgGraphData = datasetToGraph(Array(30).fill(medianCompoundRate), '119,151,170', 'Compound 30 day avg', false, 2, true, [].concat(Array(nowIndex + 1).fill(0), Array(30 - nowIndex - 1).fill(2)), 3);
    var graphData = {
      labels: arrayDays,
      datasets: [raiseGraphData, compoundGraphData, avgGraphData]
    };
    useEffectAsync(function () {
      try {
        Chart.Chart.plugins.register(todayVerticalLine);
        Chart.Chart.plugins.register(chartBackground);
        /**
         * Compound DAI rate api call, latest 30 day
         */

        return Promise.resolve(axios.get('https://api.compound.finance/api/v2/market_history/graph', {
          params: {
            asset: DAI_ADDRESS,
            min_block_timestamp: moment().subtract(30, 'day').unix(),
            max_block_timestamp: moment().unix(),
            num_buckets: 30
          }
        })).then(function (response) {
          if (response.status === 200 && response.data.supply_rates && response.data.supply_rates.length) {
            var supplyRates = response.data.supply_rates;
            var length = supplyRates.length;
            var estDataset = supplyRates.map(function (_ref2) {
              var rate = _ref2.rate;
              return rate * 100;
            });
            var currentDataset = estDataset.slice(length - nowIndex - 1);
            setFullCompoundDataset(estDataset);
            setCompoundDataset(currentDataset);
            setInterest([currentAPR, numeral(currentDataset[nowIndex] / 100).format('0.00%')]);
          }
        });
      } catch (e) {
        return Promise.reject(e);
      }
    }, []);
    options.lineAtIndex = [nowIndex];

    var updateHover = function updateHover(_event, datapoint) {
      // Return current index to be able to show tooltip outside canvas
      if (!datapoint.length) {
        setSelectedDate(dateNow);
        setInterest([currentAPR, numeral(compoundDataset[nowIndex] / 100).format('0.00%')]);
        return;
      } // Return current index to be able to show tooltip outside canvas


      if (datapoint.length) {
        var index = datapoint[0]._index;
        setSelectedDate(arrayDays[index]);
        var currentAPRGraph = index === nowIndex ? currentAPR : numeral(raiseDataset[index] / 100).format('0.00%');
        setInterest([currentAPRGraph, index > nowIndex ? medianCompoundRateNumeral : numeral(compoundDataset[index] / 100).format('0.00%')]);
      }
    };

    options.onHover = updateHover;
    return React__default.createElement(React__default.Fragment, null, React__default.createElement(Card.Grid, null, React__default.createElement(Card.Row, {
      notop: true,
      big: true,
      title: "Date",
      content: selectedDate.toLocaleDateString('es')
    }), React__default.createElement(Card.Vertical, null), React__default.createElement(Card.Row, {
      notop: true,
      big: true,
      title: "Raise",
      content: currentLoanInterest,
      contentColor: raiseGraphData.borderColor
    }), React__default.createElement(Card.Vertical, null), React__default.createElement(Card.Row, {
      notop: true,
      big: true,
      title: "Compound",
      content: compoundInterest,
      contentColor: compoundGraphData.borderColor
    })), React__default.createElement(reactChartjs2.Line, {
      data: graphData,
      options: options,
      height: 245
    }));
  };

  function _templateObject$3() {
    var data = _taggedTemplateLiteralLoose(["\n  position: relative;\n  display: flex;\n  align-items: center;\n  height: 22px;\n  &&& > div:first-child {\n    margin-left: 5px;\n  }\n  &&& > span:last-child {\n    margin-left: 9px;\n    color: #5a5a5a;\n    font-size: 14px;\n    font-weight: bold;\n  }\n"]);

    _templateObject$3 = function _templateObject() {
      return data;
    };

    return data;
  }
  var AmountComponent =
  /*#__PURE__*/
  styled.div(
  /*#__PURE__*/
  _templateObject$3());

  var Amount = function Amount(_ref) {
    var principal = _ref.principal,
        roi = _ref.roi;
    return React__default.createElement(AmountComponent, null, principal, React__default.createElement(Coin, {
      src: process.env.REACT_APP_HOST_IMAGES + "/images/ico_dai.png"
    }), roi && React__default.createElement("span", null, roi, " ROI"));
  };

  var InvestInfo = function InvestInfo(props) {
    var companyName = props.companyName,
        shortDescription = props.shortDescription,
        background = props.background,
        logo = props.logo,
        slug = props.slug,
        currentAmount = props.currentAmount,
        totalAmount = props.totalAmount,
        maxAmount = props.maxAmount,
        times = props.times,
        principal = props.principal,
        link = props.link;
    var auctionTimeLeft = times.auctionTimeLeft + " left";
    var aProps = {
      href: undefined
    };
    var toProps = {
      to: undefined
    };

    if (link) {
      aProps.href = slug;
      toProps.to = slug;
    }

    return React__default.createElement(React__default.Fragment, null, React__default.createElement(Card.Image, Object.assign({}, toProps, {
      src: background
    })), React__default.createElement(CardContent$1, Object.assign({}, toProps, {
      topRight: auctionTimeLeft,
      logo: logo
    }), React__default.createElement("a", Object.assign({}, aProps), React__default.createElement(Card.BorrowerTitle, null, companyName), React__default.createElement(Card.Description, null, shortDescription)), React__default.createElement(Card.Grid, {
      spaceBetween: true,
      alignBottom: true,
      nobottom: true
    }, React__default.createElement(Card.Header, {
      title: "Raised so far",
      amount: React__default.createElement(Amount, {
        principal: principal
      })
    }), React__default.createElement(Card.Header, {
      title: "Target",
      amount: React__default.createElement(Amount, {
        principal: maxAmount
      })
    })), React__default.createElement(Card.Progress, {
      color: "#eb3f93",
      currentAmount: currentAmount,
      totalAmount: totalAmount
    })));
  };

  function _templateObject$4() {
    var data = _taggedTemplateLiteralLoose(["\n  max-width: 372px;\n  width: 100%;\n  height: 470px;\n  margin: 0px !important;\n  padding: 0px !important;\n"]);

    _templateObject$4 = function _templateObject() {
      return data;
    };

    return data;
  }
  var CardSegment =
  /*#__PURE__*/
  styled(semanticUiReact.Segment)(
  /*#__PURE__*/
  _templateObject$4());

  var CardPlaceholder = function CardPlaceholder() {
    return React__default.createElement(CardSegment, {
      raised: true
    }, React__default.createElement(ContentLoader, {
      height: 490,
      width: 372,
      speed: 1,
      primaryColor: "#fef4f9",
      secondaryColor: "#faf1f1"
    }, React__default.createElement("rect", {
      x: "0",
      y: "0",
      rx: "5",
      ry: "5",
      width: "372",
      height: "124"
    }), React__default.createElement("rect", {
      x: "33",
      y: "99",
      rx: "0",
      ry: "0",
      width: "64",
      height: "53"
    }), React__default.createElement("rect", {
      x: "10",
      y: "180",
      rx: "3",
      ry: "3",
      width: "350",
      height: "10"
    }), React__default.createElement("rect", {
      x: "10",
      y: "200",
      rx: "3",
      ry: "3",
      width: "340",
      height: "10"
    }), React__default.createElement("rect", {
      x: "10",
      y: "220",
      rx: "3",
      ry: "3",
      width: "345",
      height: "10"
    }), React__default.createElement("rect", {
      x: "10",
      y: "240",
      rx: "3",
      ry: "3",
      width: "201",
      height: "10"
    }), React__default.createElement("rect", {
      x: "10",
      y: "410",
      rx: "3",
      ry: "3",
      width: "350",
      height: "50"
    })));
  };

  var InvestCardView = function InvestCardView(props) {
    var companyName = props.companyName,
        times = props.times,
        currentAPR = props.currentAPR,
        investorCount = props.investorCount,
        children = props.children,
        className = props.className;

    var _useState = React.useState(0),
        viewGraph = _useState[0],
        setGraphView = _useState[1];

    var onOpenGraph = function onOpenGraph() {
      setGraphView(viewGraph ? 0 : 1);
    };

    var AuctionGraph = React__default.createElement(APRGraph, Object.assign({}, props));
    var domList = [{
      key: 0,
      component: React__default.createElement(InvestInfo, Object.assign({}, props))
    }, {
      key: 1,
      component: AuctionGraph
    }];

    var _useState2 = React.useState(viewGraph),
        previousTab = _useState2[0],
        setPreviousTab = _useState2[1];

    var transitions = reactSpring.useTransition(domList[viewGraph], function (i) {
      return i.key;
    }, {
      unique: true,
      from: function from() {
        return {
          transform: "translate3d(0," + (viewGraph - previousTab) * 100 + "%, 0)",
          position: 'static'
        };
      },
      enter: {
        transform: 'translate3d(0%,0,0)',
        position: 'static'
      },
      leave: function leave() {
        return {
          transform: "translate3d(0," + (previousTab - viewGraph) * 100 + "%,0)",
          position: 'absolute'
        };
      }
    });

    if (!companyName) {
      return React__default.createElement(CardPlaceholder, null);
    }

    if (viewGraph !== previousTab) setPreviousTab(viewGraph);
    return React__default.createElement(InvestCardBody, {
      style: {
        overflow: 'hidden'
      },
      className: className
    }, React__default.createElement("div", {
      style: {
        overflow: 'hidden',
        height: '100%',
        zIndex: 3,
        position: 'relative'
      }
    }, transitions.map(function (_ref) {
      var item = _ref.item,
          key = _ref.key,
          props = _ref.props;
      return React__default.createElement(reactSpring.animated.div, {
        style: props,
        key: key
      }, item.component);
    })), React__default.createElement(CardBottom, null, React__default.createElement(Card.Grid, {
      alignCenter: true
    }, React__default.createElement(Card.Row, {
      notop: true,
      small: true,
      title: "Loan Term",
      content: times.loanTerm
    }), React__default.createElement(Card.Vertical, null), React__default.createElement(Card.Row, {
      notop: true,
      small: true,
      title: "Investors",
      content: investorCount
    }), React__default.createElement(GraphButton, {
      basic: true,
      onClick: onOpenGraph
    }, viewGraph === 0 ? React__default.createElement(React__default.Fragment, null, React__default.createElement(semanticUiReact.Icon, {
      name: "line graph",
      size: "large"
    }), React__default.createElement(Card.Row, {
      notop: true,
      small: true,
      title: "Current APR",
      content: currentAPR
    })) : React__default.createElement(React__default.Fragment, null, React__default.createElement(semanticUiReact.Icon, {
      name: "line graph",
      size: "large"
    }), React__default.createElement(SpacedDiv, null, "Go back")))), children));
  };

  var LoanState;

  (function (LoanState) {
    LoanState[LoanState["CREATED"] = 0] = "CREATED";
    LoanState[LoanState["FAILED_TO_FUND"] = 1] = "FAILED_TO_FUND";
    LoanState[LoanState["ACTIVE"] = 2] = "ACTIVE";
    LoanState[LoanState["DEFAULTED"] = 3] = "DEFAULTED";
    LoanState[LoanState["REPAID"] = 4] = "REPAID";
    LoanState[LoanState["CLOSED"] = 5] = "CLOSED";
    LoanState[LoanState["FROZEN"] = 6] = "FROZEN"; // when admin unlocks withdrawals
  })(LoanState || (LoanState = {}));

  var secondUnits = {
    month: 2592000,
    day: 86400,
    hour: 3600,
    minute: 60
  };

  var stringUnixToDate = function stringUnixToDate(stringUnix) {
    return new Date(Number(stringUnix) * 1000);
  };

  var isAuctionExpired = function isAuctionExpired(_ref) {
    var auctionEndTimestamp = _ref.auctionEndTimestamp;
    return new Date() > stringUnixToDate(auctionEndTimestamp);
  };
  var roundedTime = function roundedTime(seconds, secondUnit) {
    return Math.round(seconds / secondUnit);
  };
  var getDesiredTime = function getDesiredTime(seconds, type) {
    return pampy.match(seconds, function (s) {
      return s >= secondUnits.month;
    }, function (s) {
      return roundedTime(s, secondUnits.month) + " months";
    }, function (s) {
      return s >= secondUnits.day;
    }, function (s) {
      return roundedTime(s, secondUnits.day) + " days";
    }, function (s) {
      return s >= secondUnits.hour;
    }, function (s) {
      return roundedTime(s, secondUnits.hour) + " hours";
    }, function (s) {
      return s >= secondUnits.minute;
    }, function (s) {
      return roundedTime(s, secondUnits.minute) + " minutes";
    }, function (s) {
      return s > 0 && s < secondUnits.minute;
    }, function () {
      return '<1 minute';
    }, pampy.ANY, function () {
      return type === 'loan' ? 'Expired' : 'Auction ended';
    });
  };
  var defaultZero =
  /*#__PURE__*/
  numeral(0).format();
  var calculateFromWei = function calculateFromWei(number) {
    return number ? numeral(Number(web3Utils.fromWei(number.toString(), 'ether'))).format(numeralFormat) : defaultZero;
  };
  var calculateTimes = function calculateTimes(auction) {
    try {
      var loanTerm = getDesiredTime(Number(auction.termLength));
      var today = new Date().getTime() / 1000;
      var auctionTimeLeft = getDesiredTime(Number(auction.auctionEndTimestamp) - today);
      var loanTermLeft = getDesiredTime(Number(auction.termEndTimestamp) - today, 'loan');
      return {
        loanTerm: loanTerm,
        auctionTimeLeft: auctionTimeLeft,
        loanTermLeft: loanTermLeft
      };
    } catch (error) {
      console.error('[LOANUTILS][CalculateFromWei]', error);
      return error;
    }
  };
  var calculateInterest = function calculateInterest(auction) {
    var nowTimestamp = Date.now() / 1000;
    var maxInterestRate = Number(web3Utils.fromWei(auction.maxInterestRate.toString())) / 100;
    var minInterestRate = auction.minInterestRate ? Number(web3Utils.fromWei(auction.minInterestRate.toString())) / 100 : 0;
    var interest = 0;

    if (auction.state === LoanState.CREATED && !isAuctionExpired(auction)) {
      interest = (maxInterestRate - minInterestRate) * ((nowTimestamp - auction.auctionStartTimestamp) / (auction.auctionEndTimestamp - auction.auctionStartTimestamp)) + minInterestRate;
    } else if (auction.state === LoanState.ACTIVE || auction.state === LoanState.REPAID) {
      interest = maxInterestRate;
    } else {
      interest = maxInterestRate;
    }

    return interest;
  };
  var calculateROI = function calculateROI(auction) {
    var roi = Number(web3Utils.fromWei(auction.interestRate.toString())) * (auction.termLength / 30 / 24 / 60 / 60) / 100;
    return roi;
  };
  var calculateTotalInterest = function calculateTotalInterest(auction) {
    var interest = Number(web3Utils.fromWei(auction.interestRate.toString())) * (auction.termLength / 30 / 24 / 60 / 60 / 100);
    return interest;
  };
  var calculateTotalInterestAmount = function calculateTotalInterestAmount(auction) {
    var interest = Number(web3Utils.fromWei(auction.interestRate.toString())) * (auction.termLength / 30 / 24 / 60 / 60 / 100);
    var principal = Number(web3Utils.fromWei(auction.principal));
    return principal * interest;
  };
  var calculateAPR = function calculateAPR(auction) {
    var interest = Number(web3Utils.fromWei(auction.interestRate.toString())) / 100;
    var apr = interest * 12;
    return apr;
  };
  var calculateInvestmentReturn = function calculateInvestmentReturn(auction) {
    var lenderAmount = Number(web3Utils.fromWei(auction.lenderAmount));
    var lenderRoiAmount = lenderAmount + lenderAmount * calculateROI(auction);
    return lenderRoiAmount;
  };
  var getCalculations = function getCalculations(auction) {
    var maxAmount = calculateFromWei(auction.maxAmount);
    var maxAmountNum = Number(web3Utils.fromWei(auction.maxAmount));
    var operatorFee = calculateFromWei(auction.operatorFee);
    var operatorFeeNum = Number(web3Utils.fromWei(auction.operatorFee.toString())) / 100;
    var principal = calculateFromWei(auction.principal);
    var borrowerDebt = Number(web3Utils.fromWei(auction.borrowerDebt)).toLocaleString('es-ES');
    var maxSystemFees = numeral(maxAmountNum * operatorFeeNum).format();
    var systemFees = "-" + numeral(Number(web3Utils.fromWei(auction.principal)) * operatorFeeNum).format();
    var netBalance = calculateFromWei(auction.netBalance);

    if (auction.netBalance) {
      netBalance = numeral(Number(web3Utils.fromWei(auction.netBalance.toString()))).format();
    }

    var calculatedInterest = calculateInterest(auction);
    var expectedROI = calculatedInterest * (Number(auction.termLength) / 2628000);
    var interest = numeral(calculatedInterest).format('0.00%');
    var currentAPR = numeral(calculatedInterest * 12).format('0.00%');
    var currentAmount = numeral(principal).value();
    var totalAmount = numeral(maxAmount).value();
    var maxAPR = numeral(Number(web3Utils.fromWei(auction.maxInterestRate.toString())) / 100 * 12).format('0.00%');
    var expectedRoiFormated = numeral(expectedROI).format('0.00%');
    var lenderAmount;
    var lenderRoiAmount;
    var roi;
    var finalAPR;
    var totalInterest;
    var totalInterestAmount;

    if (auction.interestRate) {
      finalAPR = numeral(calculateAPR(auction)).format('0.00%');
      roi = numeral(calculateROI(auction)).format('0.00%');
      totalInterest = numeral(calculateTotalInterest(auction)).format('0.00%');
      totalInterestAmount = numeral(calculateTotalInterestAmount(auction)).format();
    }

    if (auction.lenderAmount) {
      lenderAmount = numeral(Number(web3Utils.fromWei(auction.lenderAmount))).format();
      var lenderRoiAmountCalc = calculateInvestmentReturn(auction);
      lenderRoiAmount = numeral(lenderRoiAmountCalc).format();
    }

    var _calculateTimes = calculateTimes(auction),
        loanTerm = _calculateTimes.loanTerm,
        auctionTimeLeft = _calculateTimes.auctionTimeLeft,
        loanTermLeft = _calculateTimes.loanTermLeft;

    var newCalcs = {
      times: {
        loanTerm: loanTerm,
        auctionTimeLeft: auctionTimeLeft,
        loanTermLeft: loanTermLeft
      },
      borrowerDebt: borrowerDebt,
      interest: interest,
      maxAmount: maxAmount,
      netBalance: netBalance,
      operatorFee: operatorFee,
      principal: principal,
      systemFees: systemFees,
      maxSystemFees: maxSystemFees,
      currentAmount: currentAmount,
      totalAmount: totalAmount,
      maxAPR: maxAPR,
      roi: roi,
      totalInterest: totalInterest,
      totalInterestAmount: totalInterestAmount,
      currentAPR: currentAPR,
      finalAPR: finalAPR,
      calculatedInterest: calculatedInterest,
      expectedROI: expectedROI,
      lenderAmount: lenderAmount,
      lenderRoiAmount: lenderRoiAmount,
      expectedRoiFormated: expectedRoiFormated
    };
    return newCalcs;
  };

  var InvestCard = function InvestCard(props) {
    var auction = props.auction,
        className = props.className,
        children = props.children,
        borrower = props.borrower;
    var link = !!props.link;
    var calculations = getCalculations(auction);

    var investProps = _extends({}, auction, {}, borrower, {}, calculations, {
      link: link
    });

    return React__default.createElement(InvestCardView, Object.assign({}, investProps, {
      className: className
    }), children);
  };

  (function (AccountType) {
    AccountType[AccountType["Borrower"] = 1] = "Borrower";
    AccountType[AccountType["Lender"] = 2] = "Lender";
  })(exports.AccountType || (exports.AccountType = {}));

  require('dotenv').config();

  exports.Card = Card;
  exports.CardPlaceholder = CardPlaceholder;
  exports.Coin = Coin;
  exports.InvestCard = InvestCard;
  exports.InvestCardView = InvestCardView;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=components.umd.development.js.map
